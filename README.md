# Open Search
This repo contains code for the open search tool that lets you search for info on repositories and contributors of an organisation.

## Setup
### Frontend
- `cd react`
- Uncomment the comments in webpack file to run locally and modify output source to `./dist`.
- Run `npm start`
- You should be able to see the changes at `localhost:8080`
- To build production files to static change the output in webpack to `../static` and run `npm run build`

### Backend
- In the original folder, run `python manage.py runserver` after building production files from frontend
- The local site can be accessed from `localhost:8000`

## Deploy
- To deploy commit the changes
- Setup heroku remote 
- `heroku git:remote -a opensearch`
- Run `git push heroku master`
- The production site can be accessed from [http://opensearch.herokuapp.com/](http://opensearch.herokuapp.com/)