import React, { useState, useEffect } from 'react';
import { Collapse, Spin } from 'antd';
import Repository from './Repository'
import { Typography } from 'antd';

const { Title } = Typography;
const { Panel } = Collapse;

const Organisation = (props) => {
    const [repos, setRepos] = useState(null);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        fetch(`/api/${props.name}/?key=${props.n}`).then((response) => {
            return response.json();
        }).then((data) => {
            setRepos(data);
            setLoading(false);
        });
    }, [props.name, props.n]);

    return (
        <div>
            <Title level={2}>Repositories of {props.name}</Title>
            <Spin spinning={loading}>
                <Collapse accordion>
                    {repos && repos.map((repo) => (
                        <Panel header={repo.name}>
                            <Repository org_name={props.name} name={repo.name} m={props.m}/>
                        </Panel>
                    ))}
                </Collapse>
            </Spin>
        </div>
    );
}

export default Organisation;