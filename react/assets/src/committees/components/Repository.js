import React, { useEffect, useState } from "react";
import { List, Avatar, Spin } from 'antd';
import { Typography } from 'antd';

const { Title } = Typography;
const Repository = (props) => {
    const [contributors, setContributors] = useState(null);
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        fetch(`/api/${props.org_name}/${props.name}/?key=${props.m}`).then((response) => {
            return response.json();
        }).then((data) => {
            setContributors(data);
            setLoading(false);
        });
    }, [props.org_name, props.name, props.m]);

    return (
        <div>
            <Title level={3}>Contributors to {props.name}</Title>
            <Spin spinning={loading}>
            {contributors && (
                <List
                    itemLayout="horizontal"
                    dataSource={contributors}
                    renderItem={contributor => (
                    <List.Item>
                        <List.Item.Meta
                        avatar={<Avatar src={contributor.avatar_url} />}
                        title={contributor.login}
                        description={`Contributions: ${contributor.contributions}`}
                        />
                    </List.Item>
                    )}
                />
            )}
            </Spin>
        </div>
    )

}

export default Repository;