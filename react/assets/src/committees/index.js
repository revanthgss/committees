import React, { useState } from "react";
import ReactDOM from "react-dom";
import { Layout, Form, Button, Input, Row, Col } from 'antd';
import Organisation from "./components/Organisation";
import { Typography } from 'antd';

const { Title } = Typography;
const { Content } = Layout;

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};
const tailLayout = {
    wrapperCol: {
        offset: 8,
        span: 16,
    },
};

const App = (props) => {
    const initialState = {
        'orgName': '',
        'n': 1,
        'm': 1
    }
    const [state, setState] = useState(initialState);

    const onFinish = (data) => {
        setState({
            'orgName': data.org_name,
            'n': data.n,
            'm': data.m
        });
    }

    return (
        <Layout className="layout" style={{ padding: '50px' }}>
            <Title>Open Search</Title>
            <Row gutter={[16,16]}>
                <Col span={24}>
                    <Content style={{ padding: '50px 250px',backgroundColor: 'white'}}>
                        <Form
                            {...layout}
                            name="Organisations"
                            initialValues={{n:1, m:1}}
                            onFinish={onFinish}
                        >
                            <Form.Item
                                label="Organisation Name:"
                                name="org_name"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please enter organisation name'
                                    }
                                ]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item
                                label="Number of popular repos"
                                name="n"
                                rules={[]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item
                                label="Number of popular committees"
                                name="m"
                                rules={[]}
                            >
                                <Input/>
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Submit
                                </Button>
                            </Form.Item>
                        </Form>
                    </Content>
                </Col>
                {state.orgName!=="" &&
                    <Col span={24}>
                        <Content style={{ padding: '50px',backgroundColor: 'white'}}>
                            <Organisation name={state.orgName} n={state.n} m={state.m}></Organisation>
                        </Content>    
                    </Col>
                }
            </Row>
            
        </Layout>
    )
}

export default App;

const wrapper = document.getElementById("container");
console.log(wrapper);
wrapper ? ReactDOM.render(<App/>, wrapper) : false;