const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const path = require("path");


const entries = {
    vendor: [
        "react",
        "react-dom",
        "antd"
    ],
    committees: path.resolve(__dirname, "./assets/src/committees/index.js")
};

module.exports = {
    entry: entries,
    output: {
        path: path.resolve(__dirname, "../static"),
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract({
                  fallback: "style-loader",
                  use: "css-loader"
                })
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader!sass-loader"
                })
            }
            // ,{
            //     test: /\.html$/,
            //     use: [
            //       {
            //         loader: "html-loader"
            //       }
            //     ]
            //   }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /en/),
        new ExtractTextPlugin("[name].css"),
        // new HtmlWebPackPlugin({
        //     template: "./assets/src/committees/index.html",
        //     filename: "./index.html"
        //   })
    ]
};