from django.conf.urls import url, include
from .views import OrgView, RepoView

urlpatterns = [
    url(r'^(?P<org>\w+)/(?P<repo>[\w-]+)/',RepoView.as_view()),
    url(r'^(?P<org>\w+)/',OrgView.as_view()),
]