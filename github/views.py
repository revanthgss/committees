from rest_framework.views import APIView
from rest_framework.response import Response
import requests
from django.conf import settings
from .utils import RequestSession, get_contributors, get_popular_repos
from .serializers import KeySerializer

class OrgView(APIView):
    '''
    Gets the popular repos of the given organisation"
    '''
    def get(self, request, org):
        serializer = KeySerializer(data=request.GET.dict())
        data = get_popular_repos(org)

        if serializer.is_valid():
            key = serializer.validated_data.get('key')
            if key<len(data):
                data = data[:key]

        return Response(data)


class RepoView(APIView):
    '''
    Gets the popular contributors for the given repo
    '''
    def get(self, request, org, repo):
        serializer = KeySerializer(data=request.GET.dict())
        data = get_contributors(org, repo)

        if serializer.is_valid():
            key = serializer.validated_data.get('key')
            if key<len(data):
                data = data[:key]

        return Response(data)