import requests
from django.conf import settings

class RequestSession():
    def __init__(self):
        self.request_session = requests.Session()
        self.request_session.headers.update({'authorizatiion': 'token '+settings.GITHUB_TOKEN})

    def get(self,url):
        return self.request_session.get(url).json()

def get_popular_repos(org):
    url = 'http://api.github.com/search/repositories?q=org:'+org+'&sort=forks'
    rs = RequestSession()
    return rs.get(url).get('items')

def get_contributors(org,repo):
    url = 'https://api.github.com/repos/'+org+'/'+repo+'/contributors'
    rs = RequestSession()
    return rs.get(url)