from rest_framework import serializers

class KeySerializer(serializers.Serializer):
    key = serializers.CharField()

    def validate_key(self, key):
        try:
            key = int(key)
            if key>0:
                return key
            else:
                raise serializers.ValidationError("Key is less than zero")
        except:
            raise serializers.ValidationError("Key is not an integer")